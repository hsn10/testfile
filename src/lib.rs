// Copyright 2023 Radim Kolar <hsn@sendmail.cz>
// SPDX-License-Identifier: MIT

//! Temporary file management for unit tests.
//!
//! Create and manage temporary files used for unit tests.
//! Managed temporary files are created in system temporary directory
//! with unique file names. They can be dereferenced as ordinary `File`
//! and are automatically deleted during `Drop`.
//!
//! Functions do not return
//! `Result <T,E>` but `panic!` allowing you to focus on your tests and
//! not on error handling.

#![forbid(missing_docs)]
#![forbid(rustdoc::missing_crate_level_docs)]
#![forbid(non_fmt_panics)]
#![warn(rustdoc::unescaped_backticks)]

//   Random name generation

use rand::prelude::*;

/**
 * Generate random number in range 1000 to 9999 inclusive using
 * provided thread bound random generator.
 */
fn generate_random(rng : &mut ThreadRng) -> u32 {
   rng.gen_range(1000..10000)
}

/**
How many times is retried attempt to
generate valid unique random test file name
before panicking.
*/
const RETRIES: u32 = 10;

/**
 * Generate random unique tmp file name.
 *
 * Returned file does not exist.
 */
pub fn generate_name() -> std::path::PathBuf {
   let mut retries = RETRIES;
   let mut rng = rand::thread_rng();
   use std::fs::OpenOptions;

   while retries > 0 {
      retries -= 1;
      let mut tmp = std::env::temp_dir();
      tmp.push(format!("TMP{}", generate_random(&mut rng)));
      match OpenOptions::new().read(true).write(true).create_new(true).open(&tmp).map(
      |f| { drop(f); let _ = std::fs::remove_file(&tmp); })
      {
         Ok(()) => return tmp,
         Err(_) => continue
      }
   }
   panic!("Can't generate suitable tmp file name");
}

include!("lib_testfile.rs");
include!("lib_testdir.rs");

#[cfg(test)]
#[path= "./lib_tests.rs"]
mod tests;
