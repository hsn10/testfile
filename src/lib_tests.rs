// Copyright 2023 Radim Kolar <hsn@sendmail.cz>
// SPDX-License-Identifier: MIT

#![allow(non_snake_case)]
#![allow(unused_mut)]
#![allow(unused_parens)]

use super::TestFile;
use super::TestDir;
use std::mem::ManuallyDrop;

static CURDIR: once_cell::sync::Lazy<std::sync::Mutex<()>> =
   once_cell::sync::Lazy::new(|| std::sync::Mutex::new(()));

/**
  Creates an open file.

  # Arguments
   * path - file name
*/
fn fileFactory(path: &std::path::Path) -> std::io::Result<std::fs::File> {
    use std::fs::OpenOptions;
    OpenOptions::new().read(true).write(true).create(true).open(path)
}

/** manually create TestFile named "basicNNNN" in temp directory. */
fn basicTestFile() -> TestFile {
    let mut tmp = std::env::temp_dir();
    let mut rng = rand::thread_rng();
    let i = super::generate_random(&mut rng);
    tmp.push(format!("basic{}",i));
    let file = fileFactory(&tmp).unwrap();
    TestFile { file:ManuallyDrop::new(file), path:tmp }
}

/** manually create TestDir named "basicdirNNN" in temp directory. */
fn basicTestDir() -> TestDir {
    let mut tmp = std::env::temp_dir();
    let mut rng = rand::thread_rng();
    let i = super::generate_random(&mut rng);
    tmp.push(format!("basicdir{}",i));
    let _ = std::fs::create_dir_all(&tmp).unwrap();
    TestDir { files:ManuallyDrop::new(Vec::new()), path:tmp }
}

/** check create file from String content */
#[test]
fn testFromStr() {
    let mut tf : TestFile = super::from("ABC");
    assert!(tf.path.exists());
    // read up string
    let mut s = String::new();
    let rc = <std::fs::File as std::io::Read>::read_to_string(&mut tf, &mut s);
    assert!( rc.is_ok() );
    assert_eq!( s, "ABC" );
    // seek to a beginning
    use std::io::Seek;
    let rc = &tf.rewind();
    assert!( rc.is_ok() );
    // read string again
    s.clear();
    let rc = <std::fs::File as std::io::Read>::read_to_string(&mut tf, &mut s);
    assert!( rc.is_ok() );
    assert_eq!( s, "ABC" );
}

/** check create TestFile from an existing file */
#[test]
fn testFromFile() {
    let mut tmp = std::env::temp_dir(); tmp.push("an-existing-file");
    let mut f = std::fs::File::create(&tmp).unwrap();
    use std::io::Write;
    use std::io::Seek;
    let _ignored = f.write("1234".as_bytes());
    drop(f);
    let mut tf: TestFile = super::from_file(&tmp);
    /* path must be same */
    assert_eq!(tmp,tf.path);
    /* file must exist */
    assert!(tmp.exists());
    assert!(&tf.path.exists());
    /* check content */
    assert!(&tf.rewind().is_ok());
    let mut s = String::with_capacity(10);
    let rc = <std::fs::File as std::io::Read>::read_to_string(&mut tf, &mut s);
    assert!( rc.is_ok() );
    assert_eq! ( s, "1234" );
    /* drop TestFile */
    drop(tf);
    /* an existing file should be deleted */
    assert!(!tmp.exists());
}

/** check create TestDir from an existing dir */
#[test]
fn testFromDir() {
    /* create source directory for a test */
    let mut tmp = std::env::temp_dir(); tmp.push("an-existing-dir");
    assert!( std::fs::create_dir_all(&tmp).is_ok() );
    assert!( tmp.exists() );
    assert!( tmp.is_dir() );
    /* create TestDir */
    let mut td: TestDir = super::from_dir(&tmp);
    /* path must be same */
    assert_eq!(tmp,td.path);
    /* file must exist */
    assert!(tmp.exists());
    assert!(&td.path.exists());
    /* drop TestDir */
    drop(td);
    /* an existing dir should be deleted */
    assert!(!tmp.exists());
}

/** check create TestDir from an existing dir */
#[test]
fn testFromCurDir() {
    /* create empty source directory for a test */
    let mut wrk = std::env::temp_dir(); wrk.push("wrk-dir");
    assert!( std::fs::create_dir_all(&wrk).is_ok() );
    assert! ( wrk.exists() );
    /* grab CD mutex */
    #[allow(unused_variables)]
    let guard = CURDIR.lock().unwrap();
    let mut curdir = std::path::PathBuf::new();
    /* save current directory */
    if let Ok(cd) = std::env::current_dir() {
       curdir.push(cd);
    } else {
       drop(guard);
       let _ = std::fs::remove_dir_all(wrk);
       panic!("Can not get env::curdir");
    }
    /* change current directory to empty */
    if let Err(_) = std::env::set_current_dir(&wrk) {
       drop(guard);
       let _ = std::fs::remove_dir_all(wrk);
       panic!("Can not set env::curdir");
    }
    /* create TestDir from "." */
    let mut td: TestDir = super::from_dir(".");
    /* TestDir path must be expanded from "." to be independent of curdir */
    assert_ne!(".", td.path.to_str().unwrap());
    /* directory must exists */
    assert!(&td.path.exists());
    assert!(&td.path.is_dir());
    /* td.path should be same as wrk */
    let tmp = std::path::PathBuf::from(&td.path);
    assert_eq! ( tmp, wrk );
    // std::env::set_current_dir(&curdir);
    /* drop TestDir while our current directory == td */
    drop(td);
    /* an existing dir should be deleted */
    assert!(!wrk.exists());
    assert!(!tmp.exists());
    /* restore curdir */
    if let Err(_) = std::env::set_current_dir(&curdir) {
       // panicking while holding guard.
       // poisons mutex because CD change can't be reverted
       panic!("Can't restore curdir.");
    }
}


/** check if generated name is in temp_dir() */
#[test]
fn testGenerateName() {
   let n = super::generate_name();
   assert! (String::from(n.to_str().unwrap()).starts_with(std::env::temp_dir().to_str().unwrap()));
   let mut f = std::fs::File::create(&n);
   assert!(f.is_ok());
   drop(f);
   assert!(std::fs::remove_file(&n).is_ok());
}

/**
Check if random number returned by generate_random helper is
in 1000 to 9999 inclusive range.
*/
#[test]
fn testGenerateRandom() {
   let mut g = rand::thread_rng();
   for _ in (1..300) {
      let n = super::generate_random(&mut g);
      assert! (n >= 1000);
      assert! (n < 10000);
   }
}

//  TestFile

#[cfg(test)]
#[path= "./lib_tests_deref.rs"]
mod deref;

#[cfg(test)]
#[path= "./lib_tests_as.rs"]
mod as_;

#[cfg(test)]
#[path= "./lib_tests_from.rs"]
mod from_;

#[cfg(test)]
#[path= "./lib_tests_testfile.rs"]
mod stru_testfile;

// TestDir

#[cfg(test)]
#[path= "./lib_tests_testdir.rs"]
mod stru_testdir;

#[cfg(test)]
#[path= "./lib_testsdir_as.rs"]
mod dir_as;

#[cfg(test)]
#[path= "./lib_testsdir_deref.rs"]
mod dir_deref;

#[cfg(test)]
#[path= "./lib_testsdir_from.rs"]
mod dir_from;
