// Copyright 2023 Radim Kolar <hsn@sendmail.cz>
// SPDX-License-Identifier: MIT

/**
 * Main TestDir structure.
 *
 * Contains two private fields: temporary files and `PathBuf` with directory name
 * used for deleting directory on `Drop`.
 *
*/
pub struct TestDir {
      files : ManuallyDrop<Vec<TestFile>>,
      path  : std::path::PathBuf
}

/**
 * Removes TestDir from disk on `Drop`.
 *
 * If removal fails it is not retried and no error message is printed.
*/
impl Drop for TestDir {
    fn drop(&mut self) {
        unsafe { ManuallyDrop::drop(&mut self.files) };
        if let Err(_) = std::fs::remove_dir_all(&self.path) {
           // directory removal failed.
           // if self.path is curdir go to upper directory and retry
           if let Ok(curdir) = std::env::current_dir() {
              if curdir.eq(&self.path) {
                 self.path.parent().map(|ud| if let Ok(_) = std::env::set_current_dir(ud) {
                                                  let _ = std::fs::remove_dir_all(&self.path);
                                             });
              }
           }
        }
    }
}

/**
 * Turns an existing directory into TestDir.
 *
 * Directory must exist.
*/
pub fn from_dir(path: impl AsRef<std::path::Path>) -> TestDir {
      use std::path::PathBuf;

      // TODO: add check if directory is writable
      let p = path.as_ref();

      if p.exists() && p.is_dir() {
         /* this unwrap is safe because we are converting constant */
         let p2: PathBuf = <PathBuf as std::str::FromStr>::from_str(".").unwrap();
         /* check if p = "." */
         if p.eq(&p2) {
            /* from_dir invoked with current dir "." we need to expand it */
            if let Ok(curdir) = std::env::current_dir() {
               TestDir { files: ManuallyDrop::new(Vec::new()), 
                         path: { let mut p3 = PathBuf::new(); p3.push(curdir); p3 }
                       }
            } else {
               panic!("Can not get current directory.");
            }
         } else {
            TestDir { files: ManuallyDrop::new(Vec::new()), 
                      path: { let mut p2 = std::path::PathBuf::new(); p2.push(p); p2 }
                    }
         }
      } else {
         panic!("Directory doesn't exists.")
      }
}

/**
 * Creates an empty directory.
*/
pub fn empty_dir() -> TestDir {
   let mut retries = RETRIES;
   let mut rng = rand::thread_rng();

   while retries > 0 {
      retries -= 1;
      let mut tmp = std::env::temp_dir();
      tmp.push(format!("TMPDIR{}", generate_random(&mut rng)));
      if tmp.exists() == false {
         match std::fs::create_dir_all(&tmp) {
            Ok(_) => return TestDir { files: ManuallyDrop::new(Vec::new()), path: tmp },
            Err(_) => continue
         }
      }
   }
   panic!("Can't create temporary directory")
}

include!("lib_testdir_deref.rs");
include!("lib_testdir_from.rs");
include!("lib_testdir_as.rs");
