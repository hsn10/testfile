// Copyright 2023 Radim Kolar <hsn@sendmail.cz>
// SPDX-License-Identifier: MIT

use super::*;

/*    F R O M    V A L U E    C O N V E R S I O N S    */

/** String */
#[test]
fn testTraitFromString() {
    let testString : String = "aloha.".to_string();
    let testStringCopy : String = String::from(&testString);

    // construct TestFile using From trait <String>
    let mut tf = TestFile::from(testString);

    // read string from disk and compare
    let mut s = String::new();
    let rc = <std::fs::File as std::io::Read>::read_to_string(&mut tf, &mut s);
    assert!( rc.is_ok() );
    assert_eq!( s, testStringCopy );
}

/** &String */
#[test]
fn testTraitFromStringRef() {
    let testString : String = "aloha.melo".to_string();

    // construct TestFile using From trait <&String>
    let mut tf = TestFile::from(&testString);

    // read string from disk and compare
    let mut s = String::new();
    let rc = <std::fs::File as std::io::Read>::read_to_string(&mut tf, &mut s);
    assert!( rc.is_ok() );
    assert_eq!( s, testString );
}

/** &str */
#[test]
fn testTraitFromStrRef() {
    let testStr : &str = "aloha.";

    // construct TestFile using From trait <&str>
    let mut tf = TestFile::from(testStr);

    // read file from disk and compare
    let mut s = String::new();
    let rc = <std::fs::File as std::io::Read>::read_to_string(&mut tf, &mut s);
    assert!( rc.is_ok() );
    assert_eq!( s, testStr );
}

/** &PathBuf */
#[test]
fn testTraitFromPathBufRef() {
    let testString : String = "gula.belo".to_string();
    let init = TestFile::from(&testString);

    let buf : &std::path::PathBuf = <TestFile as AsRef<std::path::PathBuf>>::as_ref(&init);
    // construct TestFile using From trait <&PathBuf>
    let mut tf = TestFile::from(buf);

    // read string from disk and compare
    let mut s = String::new();
    let rc = <std::fs::File as std::io::Read>::read_to_string(&mut tf, &mut s);
    assert!( rc.is_ok() );
    assert_eq!( s, testString );
}

/** PathBuf */
#[test]
fn testTraitFromPathBuf() {
    let testString : String = "gula.belo".to_string();
    let init = TestFile::from(&testString);

    let buf: std::path::PathBuf = <TestFile as AsRef<std::path::PathBuf>>::as_ref(&init).to_path_buf();
    // construct TestFile using From trait <PathBuf>
    let mut tf = TestFile::from(buf);

    // read string from disk and compare
    let mut s = String::new();
    let rc = <std::fs::File as std::io::Read>::read_to_string(&mut tf, &mut s);
    assert!( rc.is_ok() );
    assert_eq!( s, testString );
}

/** &Path */
#[test]
fn testTraitFromPathRef() {
    let testString : String = "gula.belo".to_string();
    let init = TestFile::from(&testString);

    let buf: &std::path::Path = <TestFile as AsRef<std::path::Path>>::as_ref(&init);
    // construct TestFile using From trait <&Path>
    let mut tf = TestFile::from(buf);

    // read string from disk and compare
    let mut s = String::new();
    let rc = <std::fs::File as std::io::Read>::read_to_string(&mut tf, &mut s);
    assert!( rc.is_ok() );
    assert_eq!( s, testString );
}
