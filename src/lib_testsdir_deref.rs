// Copyright 2023 Radim Kolar <hsn@sendmail.cz>
// SPDX-License-Identifier: MIT

use super::*;

/*    D E R E F E R E N C E    T E S T I N G     */

#[test]
fn canBeDereferencedAsPath() {
    let basicTestDir = basicTestDir();
    let _file: &std::path::Path = &basicTestDir;
}
