// Copyright 2023 Radim Kolar <hsn@sendmail.cz>
// SPDX-License-Identifier: MIT

//  implementing different kinds of From value conversions

/**
 * Converts String to TestFile
*/
impl From<String> for TestFile {
    fn from(content: String) -> Self {
        self::from(content)
    }
}

/**
 * Converts &String to TestFile
*/
impl From<&String> for TestFile {
    fn from(content: &String) -> Self {
        self::from(content)
    }
}

/**
 * Converts &str to TestFile
*/
impl From<&str> for TestFile {
    fn from(content: &str) -> Self {
        self::from(content)
    }
}

// we can not have:
//    From <AsRef<str>> - no generics allowed
//    str - doesn't have a size known at compile-time
//    Path - doesn't have a size known at compile-time

/**
 * Converts PathBuf to TestFile
*/
impl From <std::path::PathBuf> for TestFile {
    fn from(buf: std::path::PathBuf) -> Self {
        from_file(buf)
    }
}

/**
 * Converts &PathBuf to TestFile
*/
impl From <&std::path::PathBuf> for TestFile {
    fn from(buf: &std::path::PathBuf) -> Self {
        from_file(buf)
    }
}

/**
 * Converts &Path to TestFile
*/
impl From <&std::path::Path> for TestFile {
    fn from(buf: &std::path::Path) -> Self {
        from_file(buf)
    }
}
