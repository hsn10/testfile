// Copyright 2023 Radim Kolar <hsn@sendmail.cz>
// SPDX-License-Identifier: MIT

use super::*;

/*    D E R E F E R E N C E S    */

#[test]
fn canBeDereferencedAsFile() {
    let basicTestFile = basicTestFile();
    let _file: &std::fs::File = &basicTestFile;
}

#[test]
fn canBeDereferencedAsWritableFile() {
    let mut basicTestFile = basicTestFile();
    let _file: &mut std::fs::File = &mut basicTestFile;
}
