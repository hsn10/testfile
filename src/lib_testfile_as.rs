// Copyright 2023 Radim Kolar <hsn@sendmail.cz>
// SPDX-License-Identifier: MIT

//  implementing different kinds of dereferencing As constructors

use std::convert::{AsRef,AsMut};

impl AsRef<std::path::Path> for TestFile {
   fn as_ref(&self) -> &std::path::Path {
       &self.path
   }
}

impl AsRef<std::path::PathBuf> for TestFile {
    fn as_ref(&self) -> &std::path::PathBuf {
        &self.path
    }
}

impl AsRef<std::fs::File> for TestFile {
   fn as_ref(&self) -> &std::fs::File {
       &self.file
   }
}

impl AsMut<std::fs::File> for TestFile {
   fn as_mut(&mut self) -> &mut std::fs::File {
       &mut self.file
   }
}

impl AsMut<dyn std::io::Read> for TestFile {
   fn as_mut(&mut self) -> &mut (dyn std::io::Read + 'static) {
        self.file.deref_mut()
   }
}

impl AsMut<dyn std::io::Write> for TestFile {
   fn as_mut(&mut self) -> &mut (dyn std::io::Write + 'static) {
        self.file.deref_mut()
   }
}

impl AsRef<TestFile> for TestFile {
   fn as_ref(&self) -> &TestFile {
        self
   }
}

impl AsMut<TestFile> for TestFile {
   fn as_mut(&mut self) -> &mut TestFile {
        self
   }
}
