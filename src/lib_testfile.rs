// Copyright 2023 Radim Kolar <hsn@sendmail.cz>
// SPDX-License-Identifier: MIT

/**
 * File content initializer closure.
 * Initializes TestFile with content.
*/

/*
Trait alias is currently unstable Rust feature:
https://github.com/rust-lang/rust/issues/41517
https://doc.rust-lang.org/beta/unstable-book/language-features/trait-alias.html

#![feature(trait_alias)]
pub trait FileInitializer = Fn (&mut TestFile) -> std::io::Result<()>;
*/

use std::mem::ManuallyDrop;

/**
 * Main TestFile structure.
 *
 * Contains two private fields: opened `File` and `PathBuf` with file name
 * used for deleting file on `Drop`.
 *
 * Files are created in temporary directory with unique names.
 * Reading and writing access is supported.
 *
 * This structure can be dereferenced as ordinary `&File` and implements
 * `AsRef` / `AsMut` for `Path`, `PathBuf`, `File`, `Read`, `Write`.
 *
 * Implements additional `From` constructors for String, PathBuf, &Path, &str.
*/
pub struct TestFile {
      file : ManuallyDrop<std::fs::File>,
      path : std::path::PathBuf
}

/**
 * Removes TestFile from disk on `Drop`.
 *
 * If removal fails it is not retried and no error message is printed.
*/
impl Drop for TestFile {
    fn drop(&mut self) {
        unsafe { ManuallyDrop::drop(&mut self.file) };
        let _ = std::fs::remove_file(&self.path);
    }
}

/**
 * Create an empty test file.
*/
pub fn empty() -> TestFile {
    create(|_unused| Ok(()))
}

/**
 * Create a TestFile from str reference.
 *
 * str is file content, not file name.
*/
pub fn from<T: AsRef<str>>(content: T) -> TestFile {
    use std::io::Write;
    create(|tfile| tfile.file.write_all(content.as_ref().as_bytes()))
}

/**
 * Turns an existing file into TestFile.
 *
 * File must exist and be readable and writable.
*/
pub fn from_file(path: impl AsRef<std::path::Path>) -> TestFile {
   std::fs::OpenOptions::new().read(true).write(true).create_new(false).open(path.as_ref()).map(
      |f| TestFile{ file:ManuallyDrop::new(f), path:std::path::PathBuf::new() }).map(
      |mut tf| { tf.path.push(path.as_ref()); tf })
      .expect("File doesn't exists or its not writable.")
}

/**
 * Create test file with content supplied by initializer closure.
 *
 * If closure returns error `Result`, operation is retried several times
 * before panicking.
 */
pub fn create<I>(initializer: I) -> TestFile
       where I: Fn (&mut TestFile) -> std::io::Result<()> {

   let mut retries = RETRIES;
   let mut rng = rand::thread_rng();
   use std::fs::OpenOptions;
   use std::io::{Seek, SeekFrom};

   while retries > 0 {
      retries -= 1;
      let mut tmp = std::env::temp_dir();
      tmp.push(format!("TMP{}",generate_random(&mut rng)));
      match OpenOptions::new().read(true).write(true).create_new(true).open(&tmp).map(
      |f| TestFile{ file:ManuallyDrop::new(f), path:tmp }
      ).and_then(|mut tfile:TestFile| -> std::io::Result<TestFile> { match initializer(<TestFile as AsMut<TestFile>>::as_mut(&mut tfile)) { Ok(_) => Ok(tfile), Err(ioerr) => Err(ioerr) }}
      ).and_then(|mut tfile| match tfile.file.seek(SeekFrom::Start(0)) { Ok(_) => Ok(tfile), Err(ioerr) => Err(ioerr) }
      ).and_then(|mut tfile| match <dyn std::io::Write>::flush(<TestFile as AsMut<std::fs::File>>::as_mut(&mut tfile)) { Ok(_) => Ok(tfile), Err(ioerr) => Err(ioerr) }
      ) {
           Ok(tfile) => return tfile,
           Err(_) => continue
      }
   }
   panic!("Can't create temporary file")
}

include!("lib_testfile_from.rs");
include!("lib_testfile_as.rs");
include!("lib_testfile_deref.rs");
