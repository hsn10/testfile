// Copyright 2023 Radim Kolar <hsn@sendmail.cz>
// SPDX-License-Identifier: MIT

// Dereferencing TestDir to Path

impl Deref for TestDir {
   type Target = std::path::Path;
   fn deref(&self) -> &Self::Target { &self.path }
}
