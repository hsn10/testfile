// Copyright 2023 Radim Kolar <hsn@sendmail.cz>
// SPDX-License-Identifier: MIT

use std::ops::{Deref,DerefMut};

impl Deref for TestFile {
   type Target = std::fs::File;
   fn deref(&self) -> &Self::Target { &self.file }
}

impl DerefMut for TestFile {
   fn deref_mut(&mut self) -> &mut Self::Target { &mut self.file }
}
