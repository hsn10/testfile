// Copyright 2023 Radim Kolar <hsn@sendmail.cz>
// SPDX-License-Identifier: MIT

//  implementing different kinds of From value conversions

// we can not have:
//    Path - doesn't have a size known at compile-time

/**
 * Converts PathBuf to TestDir
*/
impl From <std::path::PathBuf> for TestDir {
    fn from(buf: std::path::PathBuf) -> Self {
        from_dir(buf)
    }
}

/**
 * Converts &PathBuf to TestDir
*/
impl From <&std::path::PathBuf> for TestDir {
    fn from(buf: &std::path::PathBuf) -> Self {
        from_dir(buf)
    }
}

/**
 * Converts &Path to TestFile
*/
impl From <&std::path::Path> for TestDir {
    fn from(buf: &std::path::Path) -> Self {
        from_dir(buf)
    }
}
