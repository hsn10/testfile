// Copyright 2023 Radim Kolar <hsn@sendmail.cz>
// SPDX-License-Identifier: MIT

use super::*;

/*    A S _    C O N V E R S I O N S    */

/* File */
#[test]
fn canBeReferencedAsFile() {
    let basicTestFile = basicTestFile();
    let _file: &std::fs::File = basicTestFile.as_ref();
}

#[test]
fn canBeReferencedAsWritableFile() {
    let mut basicTestFile = basicTestFile();
    let _file: &mut std::fs::File = basicTestFile.as_mut();
}

/* Path */
#[test]
fn canBeReferencedAsPath() {
    let basicTestFile = basicTestFile();
    let _path: &std::path::Path = basicTestFile.as_ref();
}

/* PathBuf */
#[test]
fn canBeReferencedAsPathBuf() {
    let basicTestFile = basicTestFile();
    let _pathbuf: &std::path::PathBuf = basicTestFile.as_ref();
}

/* TestFile */
#[test]
fn canBeReferencedAsTestFile() {
    let mut basicTestFile = basicTestFile();
    let _testfile: &TestFile = basicTestFile.as_ref();
}

#[test]
fn canBeReferencedAsMutableTestFile() {
    let mut basicTestFile = basicTestFile();
    let _testfile: &mut TestFile = basicTestFile.as_mut();
}

/* Read */
#[test]
fn canBeReferencedAsRead() {
    let mut basicTestFile = basicTestFile();
    let _read: &(dyn std::io::Read + 'static) = basicTestFile.as_mut();
}

/* Write */
#[test]
fn canBeReferencedAsWrite() {
    let mut basicTestFile = basicTestFile();
    let _write: &(dyn std::io::Write + 'static) = basicTestFile.as_mut();
}
