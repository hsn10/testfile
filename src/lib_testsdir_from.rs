// Copyright 2023 Radim Kolar <hsn@sendmail.cz>
// SPDX-License-Identifier: MIT

use super::*;
use super::super::empty_dir;

/*    F R O M    V A L U E    C O N V E R S I O N S    T E S T S    */

/** &PathBuf */
#[test]
fn testTraitFromPathBufRef() {
    // create empty reference testdir
    let init = empty_dir();
    // get PathBuf reference from reference testdir
    let buf : &std::path::PathBuf = <TestDir as AsRef<std::path::PathBuf>>::as_ref(&init);
    // construct TestDir using From trait <&PathBuf>
    let mut td = TestDir::from(buf);
    assert_eq! ( *init, *td);
}

/** &Path */
#[test]
fn testTraitFromPathRef() {
    // create empty reference testdir
    let init = empty_dir();
    // get Path reference from reference testdir
    let buf : &std::path::Path = <TestDir as AsRef<std::path::Path>>::as_ref(&init);

    // construct TestDir using From trait <Path>
    let mut td = TestDir::from(buf);

    assert_eq! ( *init, *td);
}

/** PathBuf */
#[test]
fn testTraitFromPathBuf() {
    // create empty reference testdir
    let init = empty_dir();
    // get PathBuf reference from reference testdir
    let buf : &std::path::PathBuf = <TestDir as AsRef<std::path::PathBuf>>::as_ref(&init);
    // create new PathBuf with content of buf
    let buf2: std::path::PathBuf = { let mut p = std::path::PathBuf::new(); p.push(buf); p };
    // construct TestDir using From trait <&PathBuf>
    let mut td = TestDir::from(buf2);
    assert_eq! ( *init, *td);
}
