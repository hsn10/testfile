// Copyright 2023 Radim Kolar <hsn@sendmail.cz>
// SPDX-License-Identifier: MIT

use super::*;

/*    A S _    C O N V E R S I O N S    T E S T S    */

/* Path */
#[test]
fn canBeReferencedAsPath() {
    let basicTestDir = basicTestDir();
    let _path: &std::path::Path = basicTestDir.as_ref();
}

/* PathBuf */
#[test]
fn canBeReferencedAsPathBuf() {
    let basicTestDir = basicTestDir();
    let _pathbuf: &std::path::PathBuf = basicTestDir.as_ref();
}

/* TestDir */
#[test]
fn canBeReferencedAsTestDir() {
    let mut basicTestDir = basicTestDir();
    let _testfile: &TestDir = basicTestDir.as_ref();
}

#[test]
fn canBeReferencedAsMutableTestDir() {
    let mut basicTestDir = basicTestDir();
    let _testfile: &mut TestDir = basicTestDir.as_mut();
}
