// Copyright 2023 Radim Kolar <hsn@sendmail.cz>
// SPDX-License-Identifier: MIT

//  implementing different kinds of dereferencing As constructors

impl AsRef<std::path::Path> for TestDir {
   fn as_ref(&self) -> &std::path::Path {
       &self.path
   }
}

impl AsRef<std::path::PathBuf> for TestDir {
    fn as_ref(&self) -> &std::path::PathBuf {
        &self.path
    }
}

impl AsRef<TestDir> for TestDir {
   fn as_ref(&self) -> &TestDir {
        self
   }
}

impl AsMut<TestDir> for TestDir {
   fn as_mut(&mut self) -> &mut TestDir {
        self
   }
}
