// Copyright 2023 Radim Kolar <hsn@sendmail.cz>
// SPDX-License-Identifier: MIT

#![allow(non_snake_case)]
#![allow(unused_mut)]
#![allow(unused_parens)]

use super::TestDir;
use std::mem::ManuallyDrop;

/** test if empty TestDir is deleted at end of block */
#[test]
fn testDirEmptyCleanup() {
    /* original directory */
    let mut tmp = std::env::temp_dir(); 
    tmp.push("cleanup-dir-test1");
    {
        /* create tmp directory */
        let _ = std::fs::create_dir_all(&tmp);
        /* create tmp2 copy and use it to create TestDir */
        let mut tmp2 = std::path::PathBuf::new();
        tmp2.push(&tmp);
        let td = TestDir { files:ManuallyDrop::new(Vec::new()), path:tmp2 };
        /* dir should exist */
        assert!(td.path.exists());
        assert!(tmp.exists());
    }
    /* dir should get deleted at end of previous block */
    assert!(!tmp.exists());
}

/** test if TestDir with a file is deleted at end of block */
#[test]
fn testDirCleanupWithFile() {
    use std::fs::OpenOptions;
    /* original directory */
    let mut tmp = std::env::temp_dir(); 
    tmp.push("cleanup-dir-test2");
    {
        /* create tmp directory */
        let _ = std::fs::create_dir_all(&tmp);
        /* create tmp2 copy and use it to create TestDir */
        let mut tmp2 = std::path::PathBuf::new();
        tmp2.push(&tmp);
        /* tmp3 is file inside testdir */
        let mut tmp3 = std::path::PathBuf::new();
        tmp3.push(&tmp);
        tmp3.push("test-file");
        assert! (OpenOptions::new().create(true).write(true).open(&tmp3).is_ok());
        assert! (tmp3.exists());
        /* create test directory */
        let td = TestDir { files:ManuallyDrop::new(Vec::new()), path:tmp2 };
        /* dir should exist */
        assert!(td.path.exists());
        assert!(tmp.exists());
    }
    /* dir should get deleted at end of previous block */
    assert!(!tmp.exists());
}
