// Copyright 2023 Radim Kolar <hsn@sendmail.cz>
// SPDX-License-Identifier: MIT

#![allow(non_snake_case)]
#![allow(unused_mut)]
#![allow(unused_parens)]

use super::TestFile;
use std::mem::ManuallyDrop;

/**
  Creates an open file.

  # Arguments
   * path - file name
*/
fn fileFactory(path: &std::path::Path) -> std::io::Result<std::fs::File> {
    use std::fs::OpenOptions;
    OpenOptions::new().read(true).write(true).create(true).open(path)
}

/** test if TestFile is deleted on end of block */
#[test]
fn testFileCleanup() {
    /* original file */
    let mut tmp = std::env::temp_dir(); 
    tmp.push("cleanup-test");
    {
        /* create tmp2 copy and use it to create TestFile */
        let mut tmp2 = std::path::PathBuf::new();
        tmp2.push(&tmp);
        let tf = TestFile { file:ManuallyDrop::new(fileFactory(&tmp2).unwrap()), path:tmp2 };
        /* file should exist */
        assert!(tf.path.exists());
        assert!(tmp.exists());
    }
    /* file should get deleted at end of previous block */
    assert!(!tmp.exists());
}

/**
Check if file is correctly flushed to disk
*/
#[test]
fn flushTest() {
    let mut tf = crate::from("ABC");
    /* must be on disk */
    assert!(tf.path.exists());
    let mut _f = std::fs::File::open(&tf.path);
    use std::io::{Seek, SeekFrom};
    /* with size 3 */
    let sz = tf.seek(SeekFrom::End(0));
    assert! ( &sz.is_ok());
    assert_eq! ( sz.unwrap(), 3u64);
}

/**
Check if file is correctly flushed to disk using BufReader
*/
#[test]
fn flushTestWithInnerBuffer() {
    let tf = crate::from("ABC");
    /* must be on disk */
    assert!(&tf.path.exists());
    fn inner<T: AsRef <std::path::Path>>(file: T) -> std::io::Result<usize> {
        let mut f = std::fs::File::open(&file)?;
        let mut jm = std::io::BufReader::new(f);
        use std::io::{Seek, SeekFrom};
        let n = jm.seek(SeekFrom::End(0))?;
        assert_eq!(n,3u64);
        Ok(n as usize)
    }
    let _unused = inner(tf);
}

/**
Check if testfile is rewinded after creation
*/
#[test]
fn testRewindAfterCreate() {
   use std::io::Seek;

   // create from str
   let mut tf = crate::from("ABCD");
   let mut rc = tf.stream_position();
   assert!(&rc.is_ok());
   assert_eq!(rc.as_ref().unwrap(), &0u64);

   // create from Trait
   tf = crate::TestFile::from("ABCD".to_string());
   rc = tf.stream_position();
   assert!(&rc.is_ok());
   assert_eq!(rc.as_ref().unwrap(), &0u64);
}
