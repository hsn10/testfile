# testfile for Rust unit tests

[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://spdx.org/licenses/MIT.html)
[![Crates.io](https://img.shields.io/crates/v/testfile.svg)](https://crates.io/crates/testfile)
[![dependency status](https://deps.rs/repo/gitlab/hsn10/testfile/status.svg)](https://deps.rs/repo/gitlab/hsn10/testfile)
[![Documentation](https://docs.rs/testfile/badge.svg)](https://docs.rs/testfile)
[![Lines of code](https://tokei.rs/b1/gitlab/hsn10/testfile?category=code)](https://github.com/XAMPPRocky/tokei)
[![Gitlab pipeline](https://gitlab.com/hsn10/testfile/badges/master/pipeline.svg?ignore_skipped=true)](https://gitlab.com/hsn10/testfile/-/pipelines)
[![Downloads](https://img.shields.io/crates/d/testfile)](https://crates.io/crates/testfile/versions)

Library for creating automaticaly deleted test files for use in your Rust unit tests.
Do not worry about manual temporary file cleanups.

Managed temporary files are created in system temporary directory with unique file names.
They can be dereferenced as ordinary `File` and are automatically deleted during `Drop`.

Functions do not return `Result <T,E>` but `panic!` allowing you to focus on your tests and not on error handling.

Nightly Rust is not required.

## Implemented traits for TestFile
1. Testfile can be dereferenced as File making it drop in replacement.
1. implements `AsRef` / `AsMut` for Read, Write, File, PathBuf, Path.
1. implements `From` String and PathBuf.
